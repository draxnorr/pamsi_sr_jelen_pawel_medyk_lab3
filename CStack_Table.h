//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CSTACK_TABLE_H
#define STACK_CSTACK_TABLE_H

#include "CAStack.h"
#include "CEmptyStackException.h"

template < typename ValueType>
class CStack_Table:public CAStack <ValueType> {
protected:
    ValueType * stack_tab;
    int stack_size;
    int stack_top;
public:
    CStack_Table(): stack_size(0), stack_top(-1), stack_tab (nullptr) {}
    ValueType& push(const ValueType& new_elem) override;
    ValueType pop() throw(CEmptyStackException) override;
    ValueType& top() throw(CEmptyStackException) override;
    virtual void grow() = 0;
    int size() const  override  {  return (stack_top + 1); }
    bool isEmpty() const override  {   return ( stack_top < 0 );   }
    ~CStack_Table();
};

template < typename ValueType>
ValueType& CStack_Table<ValueType>::push(const ValueType& new_elem){
    ++stack_top;
    if (stack_top>=stack_size)
        grow();
    stack_tab[stack_top]=new_elem;
    return stack_tab[stack_top];
}

template < typename ValueType>
ValueType CStack_Table<ValueType>::pop() throw(CEmptyStackException){
    if (isEmpty())
        throw CEmptyStackException();
    --stack_top;
    return stack_tab[stack_top+1];
}

template < typename ValueType>
ValueType& CStack_Table<ValueType>::top() throw(CEmptyStackException){
    if (isEmpty())
        throw CEmptyStackException();
    return stack_tab[stack_top];
}

template < typename ValueType >
CStack_Table<ValueType>::~CStack_Table(){
    if (stack_tab!=nullptr)
        delete [] stack_tab;
}






#endif //STACK_CSTACK_TABLE_H
