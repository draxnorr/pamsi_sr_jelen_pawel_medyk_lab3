//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CSTACK_TABLE_CONSTINC_H
#define STACK_CSTACK_TABLE_CONSTINC_H

#include <cstring>
#include "CStack_Table.h"

//stos rosnie zwiekszajac swoj rozmiar o stala wartosc = 100
//  constInc = 100;
template < typename ValueType >
class CStack_Table_ConstInc:public CStack_Table <ValueType> {
    const int constInc = 100;
protected:
    void grow() override {
        ValueType * temp_tab = new ValueType[CStack_Table<ValueType>::stack_size+constInc];
        memcpy(temp_tab, CStack_Table<ValueType>::stack_tab, CStack_Table<ValueType>::stack_size * sizeof(ValueType));
        if (CStack_Table<ValueType>::stack_tab) delete [] CStack_Table<ValueType>::stack_tab;
        CStack_Table<ValueType>::stack_tab = temp_tab;
        CStack_Table<ValueType>::stack_size+=constInc;
    }
};


#endif //STACK_CSTACK_TABLE_CONSTINC_H
