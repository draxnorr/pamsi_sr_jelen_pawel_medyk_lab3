#include <iostream>
#include "CStack_List.h"
#include "CHanoi_Tower.h"

using namespace std;



int main() {
    CHanoi_Tower _test;
    _test.start_n_ask();
    _test.display();
    _test.solve();
    _test.display();


    return 0;
}
