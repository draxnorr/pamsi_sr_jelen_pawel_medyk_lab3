//
// Created by medyk on 29.03.2017.
//

#ifndef HANOI_TOWER_CHANOI_TOWER_H
#define HANOI_TOWER_CHANOI_TOWER_H

#include "CStack_List.h"

void solve_hanoi_recurency(int n, CAStack<int>* A, CAStack<int>* B, CAStack<int>* C);

class CHanoi_Tower {
    int numb_plates;
    CAStack<int> * Tower[3];
public:
    CHanoi_Tower();
    CHanoi_Tower(const int& number_of_plates);
    void start_n_ask();
    void fill_first_tower();
    void solve()    {   solve_hanoi_recurency(numb_plates,Tower[0],Tower[1],Tower[2]); }

    friend void solve_hanoi_recurency(int n, CAStack<int>* A, CAStack<int>* B, CAStack<int>* C);
    void display();
};



#endif //HANOI_TOWER_CHANOI_TOWER_H
