//
// Created by medyk on 28.03.2017.
//
// Class Abstract Stack
#ifndef STACK_CASTACK_H
#define STACK_CASTACK_H

// Class Abstract Stack
template < typename ValueType >
class CAStack {
public:
    virtual ValueType& push(const ValueType& new_elem) = 0 ;
    virtual ValueType pop() = 0 ;
    virtual ValueType& top() = 0 ;
    virtual int size() const = 0 ;
    virtual bool isEmpty() const = 0 ;
    virtual void display_tower() const =0 ;
};




#endif //STACK_CASTACK_H
