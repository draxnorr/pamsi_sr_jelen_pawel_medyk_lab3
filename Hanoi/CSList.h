//
// Created by medyk on 09.03.2017.
//

#ifndef CSLIST_H
#define CSLIST_H
#include "CSNode.h"

// lista jednokierunkowa
template <typename ValueType >
class CSList {
    CSNode <ValueType> *head;
    CSNode <ValueType> *tail;
    int size;

public:
    CSList(): head(NULL), tail(NULL),size(0) {}
    CSList(const ValueType& value): head(head = new CSNode <ValueType> (value)), tail(head),size(1) {} //konstruktor parametryczny /wartosc pierwszego elementu

    CSNode <ValueType> * const get_head() const     {   return head;            }   //zwraca adres glowy listy /pierwszy elem/
    CSNode <ValueType> * const get_tail() const     {   return tail;            }   //zwraca adres ogona listy /ostatni elem/

    ValueType get_head_value() const        {   return head->get_value(); }
    ValueType get_tail_value() const        {   return tail->get_value(); }

    ValueType& set_head_value_by_ref()                      {   return head->set_value_by_ref();    }
    ValueType& set_tail_value_by_ref()                      {   return tail->set_value_by_ref();    }

    void push_head(const ValueType& value);
    void push_tail(const ValueType& value);
    void remove_head();
    void remove_tail();

    void remove(const int position_to_delete);
    void insert(ValueType& value,const int position_to_insert);

    virtual void display() const;

    int get_size() const    {  return size;   }
    void refresh_size();
    void dec_size()     { --size; }
    void inc_size()     { ++size; }

    virtual bool isEmpty() const { return !(head && tail); }

    virtual void delete_all_elements() {         //usuwa wszystkie elementu
        if(head) delete head;
        head = tail = NULL;
        size=0;
    }

    virtual void destroy() { delete this; }   //niszczy cala liste

    ~CSList()           {   delete_all_elements();       }   //destruktor
};

#endif //CSLIST
