//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CEMPTYSTACKEXCEPTION_H
#define STACK_CEMPTYSTACKEXCEPTION_H

#include <iostream>

class CEmptyStackException {
public:
    CEmptyStackException()  { std::cerr << "EmptyStackException";    }
};

#endif //STACK_CEMPTYSTACKEXCEPTION_H
