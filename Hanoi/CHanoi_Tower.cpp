//
// Created by medyk on 29.03.2017.
//

#include "CHanoi_Tower.h"
#include <iostream>

using namespace std;

void solve_hanoi_recurency(int n, CAStack<int>* A, CAStack<int>* B, CAStack<int>* C){
    if(n>0){
        solve_hanoi_recurency(n-1,A,C,B);
        C->push(A->pop());
        solve_hanoi_recurency(n-1,B,A,C);
    }
}

CHanoi_Tower::CHanoi_Tower():numb_plates(0) {
    for (int i=0; i<3 ; ++i)
        Tower[i]= new CStack_List<int> ();
}
CHanoi_Tower::CHanoi_Tower(const int& number_of_plates):numb_plates(number_of_plates) {
    for (int i=0; i<3 ; ++i)
        Tower[i]= new CStack_List<int> ();
    fill_first_tower();
}


void CHanoi_Tower::fill_first_tower(){
    for (int i=numb_plates; i>0; --i)
        Tower[0]->push(i);
}

void CHanoi_Tower::start_n_ask(){
    cout << "Ilosc krazkow do gry: " << '\n';
    do {
        cin >> numb_plates;
        if(numb_plates<=0)
            cout << "Niepoprawna wartosc." << '\n';
    } while (numb_plates<=0);

    fill_first_tower();
}

void CHanoi_Tower::display(){
    cout << "Wieza 1:" <<'\n';
    Tower[0]->display_tower();
    cout << "\n\n\n" ;

    cout << "Wieza 2:" <<'\n';
    Tower[1]->display_tower();
    cout << "\n\n\n" ;

    cout << "Wieza 3:" <<'\n';
    Tower[2]->display_tower();
    cout << "\n\n\n" ;


}