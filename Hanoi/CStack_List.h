//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CSTACK_LIST_H
#define STACK_CSTACK_LIST_H

#include "CEmptyStackException.h"
#include "CAStack.h"
#include "CSList.h"
#include "CSList_methods.tcc"

template < typename ValueType>
class CStack_List: protected CSList<ValueType>, public CAStack<ValueType> {
    ValueType &push(const ValueType &new_elem) override; //z CAStack
    ValueType pop() throw(CEmptyStackException) override;  //CAStack
    ValueType &top() throw(CEmptyStackException) override;  //CAStack

    int size() const override { return CSList<ValueType>::get_size();   }  //CAStack
    bool isEmpty() const override { return CSList<ValueType>::isEmpty();   }  //CAStack

    void display_tower() const { CSList<ValueType>::display();    }
};

template < typename ValueType>
ValueType CStack_List<ValueType>::pop() throw(CEmptyStackException) {  //CAStack
    if (isEmpty())
        throw CEmptyStackException();
    ValueType to_return = CSList<ValueType>::get_head_value();
    CSList<ValueType>::remove_head();
    return to_return;
}

template < typename ValueType>
ValueType& CStack_List<ValueType>::top() throw(CEmptyStackException){  //CAStack
    if (isEmpty())
        throw CEmptyStackException();
    return CSList<ValueType>::set_head_value_by_ref();
}

template < typename ValueType>
ValueType &CStack_List<ValueType>::push(const ValueType &new_elem) { //z CAStack
    CSList<ValueType>::push_head(new_elem);
    return CSList<ValueType>::set_head_value_by_ref();
}

#endif //STACK_CSTACK_LIST_H
