//
// Created by medyk on 22.03.2017.
//

#ifndef CSLIST_CSNODE_METHODS_TCC
#define CSLIST_CSNODE_METHODS_TCC

#include <iostream>
#include "CSList.h"
using namespace std;

template <typename ValueType >
void CSList <ValueType> ::push_head (const ValueType& value) {
    inc_size();
    if (!head)
        head = tail = new CSNode <ValueType> (value);
    else
        head = new CSNode <ValueType> (value,head);
}

template <typename ValueType >
void CSList <ValueType> ::push_tail (const ValueType& value) {
    inc_size();
    if (!tail)
        tail = head = new CSNode <ValueType> (value);
    else {
        tail->set_next(new CSNode<ValueType>(value));
        tail=tail->get_next();
    }
}

template <typename ValueType >
void CSList <ValueType> ::remove_head() {
    if (!head)
        cerr << "Brak elementow do usuniecia/remove_head/" << endl;
    else {
        head = head->delete_this_and_return_next_pointer();
        if (head == NULL) tail = NULL;
        dec_size();
    }
}

template <typename ValueType >
void CSList <ValueType> ::remove_tail() {
    if(!tail)
        cerr << "Brak elementow do usuniecia /remove_tail/" << endl;
    else
        remove(size-1);   // usun ostatni element
}

template <typename ValueType >
void CSList <ValueType> ::display() const {
    CSNode <ValueType> * temp = head;
    int i=0;

    while (temp) {
        cout << ++i << ". " << temp->get_value() << endl;
        temp=temp->get_next();
    }

    if(!size)
        cout << "Kontener jest pusty" << endl;
    cout << "Liczba elementow: " << size << endl;
}

template <typename ValueType >
void CSList <ValueType> ::refresh_size() {
    size=0;

    for (CSNode <ValueType> * temp = head; temp ; temp=temp->get_next())
        inc_size();
}

template <typename ValueType >
void CSList <ValueType> ::remove(const int position_to_delete){
    CSNode <ValueType> * temp = head;

    if (position_to_delete>=size || position_to_delete<0)
        cerr << "!Probujesz usunac nieistniejacy element." << endl << "!Podales liczbe niezgodna z rozmiarami tablicy. /remove(position) /" << endl;
    else {
        if (position_to_delete == 0)
            remove_head();
        else {
            for (int i = 1; i < position_to_delete; ++i)
                temp = temp->get_next();
            temp->delete_next();
            dec_size();

            if(temp->get_next()==NULL) tail= temp;
        }
    }
}

template <typename ValueType >
void CSList <ValueType> ::insert(ValueType &value,const int position_to_insert) {  // insert element to 'x' position
    if (position_to_insert > size || position_to_insert < 0)
        cerr << "Probujesz dodac element na niedozwolonej pozycji." << endl;
    else {
        CSNode <ValueType> *temp = head;
        for (int i = 0; i < position_to_insert - 1; ++i)
            temp = temp->get_next();
        temp->set_next(new CSNode<ValueType>(value, temp->get_next()));
    }
}





#endif //CSLIST_CSNODE_METHODS_TCC
