//
// Created by medyk on 29.03.2017.
//

#ifndef STACK_TESTING_H
#define STACK_TESTING_H

#include <iostream>
#include <cstdlib>
#include <ctime>

#include "CAStack.h"
#include "CAQueue.h"

#define MAX_RAND 1000   //maksymalna wylosowana liczba

template < typename ValueType >
class CTesting_time{
    double in_time, out_time; // czas zapisywania, czas wypisywania i usuwania // wymiar [ms]

public:
    double get_in_time() const { return in_time;    }
    double get_out_time() const { return out_time;  }

    void test_N_num_Stack(int N_nr,  CAStack<ValueType> *pointer);
    void test_N_num_Queue(int N_nr, CAQueue<ValueType> *pointer);
};

template < typename ValueType >
void CTesting_time<ValueType>::test_N_num_Stack(int N_nr,  CAStack<ValueType> *pointer){
    srand( time( NULL ) );
    clock_t t_start, t_stop;

    t_start=clock();
    for (int i=0; i<N_nr; ++i)
        pointer->push(std::rand()%MAX_RAND);
    t_stop=clock();
    in_time= (double) (t_stop-t_start) / CLOCKS_PER_SEC * 1000;



    std::cout << "size: " << pointer->size() << '\t';
    /*if(pointer->isEmpty())
        std::cout << "Stos pusty" << '\n';
    else {
        std::cout << "Stos NIE jest pusty" << '\n';
        std::cout << "top: " << pointer->top() << '\n';
    }
    std::cout << "POP:  " << N_nr << " liczb" << '\n';
*/

    t_start = clock();
    for (int i=0; i<N_nr; ++i)
        pointer->pop();
        //   std::cout << pointer->pop() << '\n';
    t_stop = clock();
    out_time = (double) (t_stop-t_start)/ CLOCKS_PER_SEC * 1000;

    if(pointer->isEmpty())
        std::cout << "Stos pusty" << "\n\n";

    std::cout << "STOS" << '\n' << "Liczba danych testowych: " << N_nr << '\n';
    std::cout << "Czas wprowadzania: " << in_time << "[ms]\n";
    std::cout << "Czas zdejmowania: " << out_time << "[ms]\n\n\n";

}
template < typename ValueType >
void CTesting_time<ValueType>::test_N_num_Queue(int N_nr, CAQueue<ValueType> *pointer){
    srand( time( NULL ) );
    clock_t t_start, t_stop;

    t_start=clock();
    for (int i=0; i<N_nr; ++i)
        pointer->enqueue(std::rand()%MAX_RAND);
    t_stop=clock();
    in_time= (double) (t_stop-t_start) / CLOCKS_PER_SEC * 1000;

    std::cout << "size: " << pointer->size() << '\t';
   /* if(pointer->isEmpty())
        std::cout << "Kolejka pusta" << '\n';
    else {
        std::cout << "Kolejka NIE jest pusta" << '\n';
        std::cout << "top: " << pointer->front() << '\n';
    }
    */
    t_start=clock();
    for (int i=0; i<N_nr; ++i)
        pointer->dequeue();
     //   std::cout << pointer->dequeue() << '\n';
    t_stop = clock();
    out_time = (double) (t_stop-t_start)/ CLOCKS_PER_SEC * 1000;

    if(pointer->isEmpty())
        std::cout << "Kolejka pusta" << "\n\n";

    std::cout << "KOLEJKA" << '\n' << "Liczba danych testowych: " << N_nr << '\n';
    std::cout << "Czas wprowadzania: " << in_time << "[ms]\n";
    std::cout << "Czas zdejmowania: " << out_time << "[ms]\n\n\n";

}

#endif //STACK_TESTING_H
