//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CQUEUE_LIST_H
#define STACK_CQUEUE_LIST_H

#include "CEmptyQueueException.h"
#include "CAQueue.h"
#include "CSList.h"
#include "CSList_methods.tcc"

template <typename ValueType >
class CQueue_List: protected CSList<ValueType>, public CAQueue<ValueType>  {
    ValueType &enqueue(const ValueType &new_elem) override; //z CAStack
    ValueType dequeue() throw(CEmptyQueueException) override;  //CAStack
    ValueType &front() throw(CEmptyQueueException) override;  //CAStack

    int size() const override { return CSList<ValueType>::get_size();   }  //CAStack
    bool isEmpty() const override { return CSList<ValueType>::isEmpty();   }  //CAStack
};

template < typename ValueType>
ValueType CQueue_List<ValueType>::dequeue() throw(CEmptyQueueException) {  //CAStack
    if (isEmpty())
        throw CEmptyQueueException();
    ValueType to_return = CSList<ValueType>::get_head_value();
    CSList<ValueType>::remove_head();
    return to_return;
}

template < typename ValueType>
ValueType& CQueue_List<ValueType>::front() throw(CEmptyQueueException){  //CAStack
    if (isEmpty())
        throw CEmptyQueueException();
    return CSList<ValueType>::set_head_value_by_ref();
}

template < typename ValueType>
ValueType &CQueue_List<ValueType>::enqueue(const ValueType &new_elem) { //z CAStack
    CSList<ValueType>::push_tail(new_elem);
    return CSList<ValueType>::set_tail_value_by_ref();
}

#endif //STACK_CQUEUE_LIST_H
