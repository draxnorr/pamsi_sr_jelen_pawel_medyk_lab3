//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CQUEUE_TABLE_DOUBLEINC_H
#define STACK_CQUEUE_TABLE_DOUBLEINC_H

#include <cstring>
#include "CQueue_Table.h"
#define INITIAL_NUMBER_CQ 8

template <typename ValueType >
class CQueue_Table_DoubleInc:public CQueue_Table <ValueType>  {
    const int multiplier = 2;

protected:
    void grow() override{
        int new_size;
        ValueType * temp_tab;
        int right_indexes = CQueue_Table<ValueType>::table_size-CQueue_Table<ValueType>::front_index; //druga czesc to: liczba_elementow-first_part

        if(CQueue_Table<ValueType>::table_size)
            new_size=CQueue_Table<ValueType>::table_size*2;
        else
            new_size=INITIAL_NUMBER_CQ;

        temp_tab = new ValueType[new_size];

        if(CQueue_Table<ValueType>::queue_tab) {
            for(int i=0; i<CQueue_Table<ValueType>::elem_in_queue; ++i)
                temp_tab[i]=CQueue_Table<ValueType>::queue_tab[(CQueue_Table<ValueType>::front_index+i)%CQueue_Table<ValueType>::table_size];
            delete [] CQueue_Table<ValueType>::queue_tab;
        }
        CQueue_Table<ValueType>::queue_tab = temp_tab;
        CQueue_Table<ValueType>::table_size = new_size;
        CQueue_Table<ValueType>::front_index=CQueue_Table<ValueType>::elem_in_queue;
    }
};


#endif //STACK_CQUEUE_TABLE_DOUBLEINC_H
