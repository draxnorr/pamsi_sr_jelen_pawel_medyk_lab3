//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CSTACK_STL_H
#define STACK_CSTACK_STL_H

#include <stack>
#include "CAStack.h"
#include "CEmptyStackException.h"

template < typename ValueType >
class CStack_STL: protected std::stack<ValueType>, public CAStack<ValueType> {
    std::stack <ValueType> my_stack;
public:
    ValueType& push(const ValueType& new_elem) override;
    ValueType pop() throw (CEmptyStackException) override;
    ValueType& top()  throw (CEmptyStackException) override;

    int size() const override   {   return my_stack.size(); }
    bool isEmpty() const override   {   return my_stack.empty();  }
};

template < typename ValueType >
ValueType& CStack_STL<ValueType>::push(const ValueType& new_elem) {
    my_stack.push(new_elem);
    return my_stack.top();
}

template < typename ValueType >
ValueType CStack_STL<ValueType>::pop() throw (CEmptyStackException) {
    if (isEmpty())
        throw CEmptyStackException();

    ValueType to_return = my_stack.top();
    my_stack.pop();
    return to_return;
}

template < typename ValueType >
ValueType& CStack_STL<ValueType>::top()  throw (CEmptyStackException) {
    if (isEmpty())
        throw CEmptyStackException();
    return my_stack.top();
}


#endif //STACK_CSTACK_STL_H
