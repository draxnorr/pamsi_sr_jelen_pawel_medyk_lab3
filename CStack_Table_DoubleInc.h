//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CSTACK_TABLE_DOUBLEINC_H
#define STACK_CSTACK_TABLE_DOUBLEINC_H

#include <cstring>
#define INITIAL_NUMBER_CS 8
#include "CStack_Table.h"

//stos rosnie dublujac swoj rozmiar
template <typename ValueType >
class CStack_Table_DoubleInc:public CStack_Table <ValueType> {
const int multiplier = 2;

protected:
    void grow() override{
        int new_size;

        if(CStack_Table<ValueType>::stack_size)
            new_size = CStack_Table<ValueType>::stack_size*multiplier;
        else
            new_size = INITIAL_NUMBER_CS;

        ValueType * temp_tab = new ValueType[new_size];
        memcpy(temp_tab, CStack_Table<ValueType>::stack_tab, CStack_Table<ValueType>::stack_size * sizeof(ValueType));
        if (CStack_Table<ValueType>::stack_tab) delete [] CStack_Table<ValueType>::stack_tab;
        CStack_Table<ValueType>::stack_tab = temp_tab;
        CStack_Table<ValueType>::stack_size=new_size;
    }
};







#endif //STACK_CSTACK_TABLE_DOUBLEINC_H
