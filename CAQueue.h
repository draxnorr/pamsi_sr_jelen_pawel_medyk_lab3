//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CAQUEUE_H
#define STACK_CAQUEUE_H

// Class Abstract Queue
template < typename ValueType >
class CAQueue {
public:
    virtual ValueType& enqueue(const ValueType& new_elem) = 0;
    virtual ValueType dequeue() = 0; //throws (EmptyQueueException)
    virtual ValueType& front() = 0; //throws (EmptyQueueException)
    virtual int size() const = 0;
    virtual bool isEmpty() const = 0;
};


#endif //STACK_CAQUEUE_H
