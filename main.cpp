#include <iostream>

#include "CAStack.h"
#include "CStack_Table_ConstInc.h"
#include "CStack_Table_DoubleInc.h"
#include "CStack_STL.h"
#include "CStack_List.h"

#include "CAQueue.h"
#include "CQueue_Table_ConstInc.h"
#include "CQueue_Table_DoubleInc.h"
#include "CQueue_List.h"
#include "CQueue_STL.h"

#include "Testing.h"

typedef int DataType;
using namespace std;


int main() {
    CAStack<DataType> * pointer_stack = new CStack_List <DataType>();
    CAQueue<int> * pointer_queue = new CQueue_List <DataType>();
    CTesting_time<DataType> stack_test, queue_test;
    int test_numb[]= {1000, 10000, 100000, 500000, 1000000};

    for(int i=0; i<(sizeof(test_numb)/sizeof(test_numb[0])); ++i){
        stack_test.test_N_num_Stack(test_numb[i],pointer_stack);
        queue_test.test_N_num_Queue(test_numb[i],pointer_queue);
    }

    return 0;
}