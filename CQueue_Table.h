//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CQUEUE_TABLE_H
#define STACK_CQUEUE_TABLE_H
#include "CAQueue.h"
#include "CEmptyQueueException.h"

template < typename ValueType>
class CQueue_Table:public CAQueue <ValueType>  {
protected:
    ValueType * queue_tab;
    int table_size;
    int front_index;
    int elem_in_queue;
public:
    CQueue_Table(): queue_tab (nullptr), table_size(0), front_index(-1), elem_in_queue(0)  {}
    ValueType& enqueue(const ValueType& new_elem) override;
    ValueType dequeue() throw(CEmptyQueueException) override;
    ValueType& front() throw(CEmptyQueueException) override;
    virtual void grow() = 0;
    int size() const  override  {  return elem_in_queue; }
    bool isEmpty() const override  {   return ( elem_in_queue <= 0 );   }
    ~CQueue_Table();
};

template < typename ValueType>
ValueType& CQueue_Table<ValueType>::enqueue(const ValueType& new_elem){
    ++elem_in_queue;
    if(elem_in_queue>table_size)
        grow();
    if(front_index>=table_size)
        front_index=0;
    queue_tab[front_index]=new_elem;
    return queue_tab[front_index];
}

template < typename ValueType>
ValueType CQueue_Table<ValueType>::dequeue() throw(CEmptyQueueException){
    if (isEmpty())
        throw CEmptyQueueException();
    ValueType to_return = queue_tab[front_index];

    --elem_in_queue;
    if(elem_in_queue == 0) {
        delete[] queue_tab;
        front_index=-1;
        table_size=0;
        queue_tab=nullptr;
    } else{
        ++front_index;
        if(front_index>=table_size)
            front_index=0;
    }

    return to_return;
}

template < typename ValueType>
ValueType& CQueue_Table<ValueType>::front() throw(CEmptyQueueException){
    if (isEmpty())
        throw CEmptyQueueException();
    return queue_tab[front_index];
}

template < typename ValueType >
CQueue_Table<ValueType>::~CQueue_Table(){
    if (queue_tab!=nullptr)
        delete [] queue_tab;
}




#endif //STACK_CQUEUE_TABLE_H
