//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CQUEUE_TALBE_CONSTINC_H
#define STACK_CQUEUE_TALBE_CONSTINC_H

#include <cstring>
#include "CQueue_Table.h"


template <typename ValueType >
class CQueue_Table_ConstInc:public CQueue_Table <ValueType> {
    const int constInc = 100;

protected:
    void grow() override {
        ValueType * temp_tab = new ValueType[CQueue_Table<ValueType>::table_size+constInc];
        int right_indexes = CQueue_Table<ValueType>::table_size-CQueue_Table<ValueType>::front_index; //druga czesc to: liczba_elementow-first_part

        if(CQueue_Table<ValueType>::queue_tab) {
            for(int i=0; i<CQueue_Table<ValueType>::elem_in_queue; ++i)
                temp_tab[i]=CQueue_Table<ValueType>::queue_tab[(CQueue_Table<ValueType>::front_index+i)%CQueue_Table<ValueType>::table_size];
            delete [] CQueue_Table<ValueType>::queue_tab;
        }
        CQueue_Table<ValueType>::queue_tab = temp_tab;
        CQueue_Table<ValueType>::table_size+=constInc;
        CQueue_Table<ValueType>::front_index=CQueue_Table<ValueType>::elem_in_queue;
    }
};


#endif //STACK_CQUEUE_TALBE_CONSTINC_H
