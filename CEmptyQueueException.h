//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CEMPTYQUEUEEXCEPTION_H
#define STACK_CEMPTYQUEUEEXCEPTION_H

#include <iostream>

class CEmptyQueueException {
public:
    CEmptyQueueException()  { std::cerr << "EmptyQueueException";    }
};

#endif //STACK_CEMPTYQUEUEEXCEPTION_H
