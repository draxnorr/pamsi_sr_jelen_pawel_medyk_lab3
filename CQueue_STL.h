//
// Created by medyk on 28.03.2017.
//

#ifndef STACK_CQUEUE_STL_H
#define STACK_CQUEUE_STL_H

#include <queue>
#include "CAQueue.h"
#include "CEmptyQueueException.h"

template < typename ValueType >
class CQueue_STL: protected std::queue<ValueType>, public CAQueue<ValueType>  {
    std::queue <ValueType> my_queue;
public:
    ValueType& enqueue(const ValueType& new_elem) override;
    ValueType dequeue() throw (CEmptyQueueException) override;
    ValueType& front()  throw (CEmptyQueueException) override;

    int size() const override   {   return my_queue.size(); }
    bool isEmpty() const override   {   return my_queue.empty();  }
};

template < typename ValueType >
ValueType& CQueue_STL<ValueType>::enqueue(const ValueType& new_elem) {
    my_queue.push(new_elem);
    return my_queue.back();
}

template < typename ValueType >
ValueType CQueue_STL<ValueType>::dequeue() throw (CEmptyQueueException) {
    if (isEmpty())
        throw CEmptyQueueException();

    ValueType to_return = my_queue.front();
    my_queue.pop();
    return to_return;
}

template < typename ValueType >
ValueType& CQueue_STL<ValueType>::front()  throw (CEmptyQueueException) {
    if (isEmpty())
        throw CEmptyQueueException();
    return my_queue.front();
}



#endif //STACK_CQUEUE_STL_H
