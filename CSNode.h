//
// Created by medyk on 09.03.2017.
//

#ifndef CSNODE_H
#define CSNODE_H
#include <iostream>

template <typename ValueType >
class CSNode {
    CSNode* next;
    ValueType value;
public:
    CSNode(): next(NULL),value(0) {}                                //konstruktory
    CSNode(const ValueType& value): next(NULL), value(value) {}
    CSNode(const ValueType& value, CSNode* p_next): next(p_next), value(value) {}
    CSNode(CSNode& copy): next(copy.get_next()), value(copy.get_value()) {}

    ValueType   get_value() const               { return value;     }   //gettery i settery
    void        set_value(ValueType value)      { value = value;    }
    ValueType&  set_value_by_ref()              { return value;     }

    CSNode * get_next() const    { return next;          }
    void set_next(CSNode* p_next)       { next = p_next;        }

    void delete_next();                                             //usuwa tylko kolejny element
    CSNode * delete_this_and_return_next_pointer();                  //usuwa biezacy element i zwraca adres nastepnika

    ~CSNode()                           { if(next) delete next;}    // usuwa wszystkie elementy nastepujace po nim
};

template <typename ValueType >
void CSNode <ValueType> ::delete_next(){
    if(next==NULL) std::cerr << "Usuwanie nieistniejacego eleemntu /class CSNode/." << std::endl;
    else {
        CSNode<ValueType> *to_delete = next;                    // wskaznik na value do usuniecia (na nastepnik)

        next = to_delete->get_next();                   // zapamietaj adres po nastepniku
        to_delete->set_next(NULL);                              // niech usuwany nastepnik zapomni co po nim
        delete to_delete;                                       // zwolnij pamiec po nastepniku
    }
}

template <typename ValueType >
CSNode<ValueType>* CSNode<ValueType>::delete_this_and_return_next_pointer() {
    CSNode <ValueType>* temp_to_return=next;   //zapamietaj next, aby zwrocic

    next=NULL;
    delete this;

    return temp_to_return;
}

#endif //CSNode_H
